package com.example.employeeroosterdemo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CalendarView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.example.employeeroosterdemo.R;
import com.example.employeeroosterdemo.data.Rooster;
import com.example.employeeroosterdemo.data.RoosterDB;
import com.example.employeeroosterdemo.ui.EmployeeSelectionFragment;
import com.google.android.material.textfield.TextInputEditText;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final RoosterDB roosterDB = RoosterDB.getInstance(this);
        if(roosterDB.getRoosterDAO().getRoosters().size() == 0){
            ArrayList<Rooster> r = new ArrayList<>();
            Calendar calendar = Calendar.getInstance();
            calendar.set(2020,7,1);
            Date sdate = new Date(calendar.getTimeInMillis());
            calendar.set(2020,7,10);
            Date edate = new Date(calendar.getTimeInMillis());
            r.add(new Rooster(1,1,1,sdate, edate));

            calendar.set(2020,7,10);
            sdate = new Date(calendar.getTimeInMillis());
            calendar.set(2020,7,19);
            edate = new Date(calendar.getTimeInMillis());
            r.add(new Rooster(2,2,2,sdate, edate));

            calendar.set(2020,7,19);
            sdate = new Date(calendar.getTimeInMillis());
            calendar.set(2020,7,25);
            edate = new Date(calendar.getTimeInMillis());
            r.add(new Rooster(3,3,3,sdate, edate));

            calendar.set(2020,7,25);
            sdate = new Date(calendar.getTimeInMillis());
            calendar.set(2020,7,29);
            edate = new Date(calendar.getTimeInMillis());
            r.add(new Rooster(4,4,4,sdate, edate));
            r.add(new Rooster(5,5,5,sdate, edate));
            roosterDB.getRoosterDAO().insertAll(r);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        CalendarView cl = (CalendarView)findViewById(R.id.calendarView);
        cl.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int i, int i1, int i2) {
                Toast.makeText(getApplicationContext(), ""+String.valueOf(i)+String.valueOf(i1)+String.valueOf(i2), Toast.LENGTH_SHORT).show();
                FragmentManager fm = getSupportFragmentManager();
                EmployeeSelectionFragment employeeSelectionFragment = new EmployeeSelectionFragment(i,i1,i2);
//                employeeSelectionFragment.
                employeeSelectionFragment.show(fm,"fr");
            }
        });



        /// Last min UI CHANGES TO FACILITATE LINKING TO SERVER
        ImageView revert = findViewById(R.id.imageView);
        LinearLayout ll = findViewById(R.id.EndPtLayout);
        TextInputEditText t = findViewById(R.id.endpoint);
        t.setText(Configurations.ENDPOINT_URL);
        revert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Configurations.ENDPOINT_URL = "http://192.168.0.109:8080/";
                t.setText(Configurations.ENDPOINT_URL);
            }
        });
        t.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Configurations.ENDPOINT_URL = charSequence.toString();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        Switch netSw = findViewById(R.id.switch1);
        netSw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                    ll.setVisibility(View.VISIBLE);
                else
                    ll.setVisibility(View.GONE);
                Configurations.SWITCH_TO_NETWORK = b;

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}