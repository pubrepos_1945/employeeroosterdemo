package com.example.employeeroosterdemo.network;

import android.util.Log;

import com.example.employeeroosterdemo.data.Rooster;

import java.io.IOException;
import java.sql.Date;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RestService {
    private final String API_ENDPOINT;

    public RestService(String API_ENDPOINT) {
        this.API_ENDPOINT = API_ENDPOINT;
    }

    public List<String> GetAllLocations(Callback<String> cb){
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(ScalarsConverterFactory.create())
                .baseUrl(API_ENDPOINT)
                .build();
        RoosterService roosterService = retrofit.create(RoosterService.class);
        try {
            Call<String> call = roosterService.getAllLocations();
            call.enqueue(cb);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void getEmployeesAgainstLocAndDate(int loc, Date date, Callback<String> cb) {
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(ScalarsConverterFactory.create())
                .baseUrl(API_ENDPOINT)
                .build();
        RoosterService roosterService = retrofit.create(RoosterService.class);
        try {
            Call<String> call = roosterService.getEmployeesAgainstLocAndDate(loc, date);
            call.enqueue(cb);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

