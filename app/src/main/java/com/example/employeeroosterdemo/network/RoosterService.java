package com.example.employeeroosterdemo.network;

import java.sql.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RoosterService {
    @GET("/")
    Call<String> getAllLocations();
    @GET("/getEmps")
    Call<String> getEmployeesAgainstLocAndDate(@Query("locid")int locid, @Query("date") Date date);
}
