package com.example.employeeroosterdemo.viewmodels;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.employeeroosterdemo.data.RoosterRepository;

public class EmployeeSelectionViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private RoosterRepository roosterRepository;


    public EmployeeSelectionViewModelFactory(RoosterRepository roosterRepository) {
        super();
        this.roosterRepository = roosterRepository;
    }

    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new EmployeeSelectionViewModel(roosterRepository);
    }

}
