package com.example.employeeroosterdemo.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.employeeroosterdemo.Configurations;
import com.example.employeeroosterdemo.data.Locations;
import com.example.employeeroosterdemo.data.RoosterRepository;
import com.example.employeeroosterdemo.network.RestService;
import com.example.employeeroosterdemo.utilities.AppExecutors;

import java.sql.Date;
import java.util.List;

import retrofit2.Callback;

public class EmployeeSelectionViewModel extends ViewModel {

    private  RoosterRepository roosterRepository;

    public EmployeeSelectionViewModel(RoosterRepository roosterRepository) {
        this.roosterRepository = roosterRepository;
    }

    ///fetch list of locations to bind to the spinner
    public List<String> getAllLocations( Callback<String> cb){
        if(Configurations.SWITCH_TO_NETWORK){
            RestService restService =  new RestService(Configurations.ENDPOINT_URL);
            restService.GetAllLocations(cb);
            return null;
        }
        return roosterRepository.getAllLocations();
    }

    public List<String> getEmployeesOnDate(Date date, int loc, Callback<String> cb) {
        if(Configurations.SWITCH_TO_NETWORK) {
            RestService restService = new RestService(Configurations.ENDPOINT_URL);
            restService.getEmployeesAgainstLocAndDate(loc, date, cb);
            return null;
        }
        return roosterRepository.getEmployeesOnDate(date,loc);
    }

    ///fetch list of employees based on location and date for binding to the listview


}
