package com.example.employeeroosterdemo.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.employeeroosterdemo.R;
import com.example.employeeroosterdemo.data.RoosterDB;
import com.example.employeeroosterdemo.data.RoosterRepository;
import com.example.employeeroosterdemo.viewmodels.EmployeeSelectionViewModel;
import com.example.employeeroosterdemo.viewmodels.EmployeeSelectionViewModelFactory;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmployeeSelectionFragment extends DialogFragment {
    final private Date selectedDate;
    public EmployeeSelectionFragment(int i, int i1,int i2) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(i,i1,i2);
        selectedDate = new Date(calendar.getTimeInMillis());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.employee_picker,container);
        return view;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDialog().setTitle("Select the Location to fetch employees.");
        TextView seldate=  view.findViewById(R.id.selDate);
        SimpleDateFormat d = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = d.format(selectedDate);
        seldate.setText("Selected Date is : " + dateString);
        final RoosterDB roosterDB = RoosterDB.getInstance(getActivity());
        EmployeeSelectionViewModelFactory factory =
                new EmployeeSelectionViewModelFactory(RoosterRepository.getInstance(roosterDB.getRoosterDAO(), roosterDB.getEmployeeDAO(), roosterDB.getLocationsDAO()));
        EmployeeSelectionViewModel viewModel = factory.create(EmployeeSelectionViewModel.class);

        Callback<String> cb = new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.i("","");
                List<String> list = new ArrayList<>();
                String res =  response.body();
                if(res!= null &&res.contains(",")){
                    list = Arrays.asList(res.split(","));
                }else
                    list.add(res);
                PopulateDropDown(list, view, viewModel, getActivity());
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.i("","");
                Toast.makeText(getContext(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        };

        List<String> l = viewModel.getAllLocations(cb);
        if(l != null)
            PopulateDropDown(l, view, viewModel, getActivity());
    }

    private void PopulateDropDown(List<String> list, View view, EmployeeSelectionViewModel viewModel, Context context) {
        Spinner spinner= (Spinner)view.findViewById(R.id.spinner);

        ArrayAdapter<String> aa = new ArrayAdapter<String>(context,android.R.layout.simple_spinner_item, list);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(aa);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View _view, int i, long l) {
                Callback<String> cb = new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        Log.i("","");
                        List<String> list = new ArrayList<>();
                        String res =  response.body();
                        if(res.contains(",")){
                            list = Arrays.asList(res.split(","));
                        }else
                            list.add(res);
                        PopulateEmpList(list, view, getActivity());
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Log.i("","");
                        Toast.makeText(getContext(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                };
                List<String> list = viewModel.getEmployeesOnDate(selectedDate, i+1, cb);
                if(list != null)
                    PopulateEmpList(list, view, getActivity());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void PopulateEmpList(List<String> list, View view, Context context) {
        RecyclerView recyclerView = view.findViewById(R.id.employees);
        ArrayAdapter<String> aa = new ArrayAdapter<String>(context,android.R.layout.list_content, list);
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);


        MyAdapter mAdapter = new MyAdapter(list);
        recyclerView.setAdapter(mAdapter);
    }


    @Override
    public void dismiss() {
        super.dismiss();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    public static class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
        private List<String> mDataset;

        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder
        public static class MyViewHolder extends RecyclerView.ViewHolder {
            // each data item is just a string in this case
            public TextView textView;
            public MyViewHolder(TextView v) {
                super(v);
                textView = v;
            }
        }

        // Provide a suitable constructor (depends on the kind of dataset)
        public MyAdapter(List<String> myDataset) {
            mDataset = myDataset;
        }

        // Create new views (invoked by the layout manager)
        @Override
        public MyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
            // create a new view
            TextView v = (TextView) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.my_text_view, parent, false);
            MyViewHolder vh = new MyViewHolder(v);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            // - get element from your dataset at this position
            // - replace the contents of the view with that element
            holder.textView.setText(mDataset.get(position));

        }

        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return mDataset.size();
        }
    }



}

