package com.example.employeeroosterdemo.data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface EmployeeDAO {
        @Query("SELECT * FROM Employee ORDER BY EmployeeName")
        LiveData<List<Employee>> getEmployees();

        @Query("SELECT * FROM Employee WHERE id = :Id ORDER BY EmployeeName")
        LiveData<List<Employee>> getEmployeesWithId(int Id);

        @Query("SELECT * FROM Employee WHERE id = :EmployeeId")
        LiveData<Employee> getEmployee(String EmployeeId);

        @Query("SELECT * FROM Employee WHERE id in (:EmployeeIds)")
        List<Employee> getEmployeesIn(String EmployeeIds);

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insertAll(List<Employee> Employees);

}
