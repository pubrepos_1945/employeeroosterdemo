package com.example.employeeroosterdemo.data;


import android.content.Context;
import android.provider.SyncStateContract;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.employeeroosterdemo.Configurations;

/**
 * The Room database for this app
 */
@Database(entities = {Employee.class, Rooster.class, Locations.class}, version = 1, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class RoosterDB extends RoomDatabase {
    public abstract EmployeeDAO getEmployeeDAO();
    public abstract RoosterDAO getRoosterDAO();
    public abstract LocationDAO getLocationsDAO();

    private static volatile RoosterDB instance;

    public static RoosterDB getInstance(Context context) {
        if (instance == null) {
            synchronized (RoosterDB.class) {
                instance = buildDatabase(context);
            }
        }
        return instance;
    }

    // Create and pre-populate the database.
    private static RoosterDB buildDatabase(Context context) {
        return Room.databaseBuilder(context, RoosterDB.class, Configurations.DATABASE_NAME)
                .addCallback(new RoomDatabase.Callback() {
                    @Override
                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
                        super.onCreate(db);
                        ///populate the db here
                        db.execSQL("INSERT INTO EMPLOYEE VALUES(1,'Aashish')");
                        db.execSQL("INSERT INTO EMPLOYEE VALUES(2,'Barman')");
                        db.execSQL("INSERT INTO EMPLOYEE VALUES(3,'Charlie')");
                        db.execSQL("INSERT INTO EMPLOYEE VALUES(4,'Echo')");
                        db.execSQL("INSERT INTO EMPLOYEE VALUES(5,'Foxtrot')");
                        db.execSQL("INSERT INTO Locations VALUES(1,'Location1')");
                        db.execSQL("INSERT INTO Locations VALUES(2,'Location2')");
                        db.execSQL("INSERT INTO Locations VALUES(3,'Location3')");
                        db.execSQL("INSERT INTO Locations VALUES(4,'Location4')");
                        db.execSQL("INSERT INTO Locations VALUES(5,'Location5')");

//                        db.execSQL("INSERT INTO Rooster(Id, LocationId, EmployeeId,startDate, endDate) VALUES(1,1,1,'2020-08-01','2020-08-09')");
//                        db.execSQL("INSERT INTO Rooster(Id, LocationId, EmployeeId,startDate, endDate) VALUES(2,1,2,'2020-08-01','2020-08-09')");
//                        db.execSQL("INSERT INTO Rooster(Id, LocationId, EmployeeId,startDate, endDate) VALUES(3,2,3,'2020-08-10','2020-08-19')");
//                        db.execSQL("INSERT INTO Rooster(Id, LocationId, EmployeeId,startDate, endDate) VALUES(4,3,4,'2020-08-20','2020-08-29')");
//                        db.execSQL("INSERT INTO Rooster(Id, LocationId, EmployeeId,startDate, endDate) VALUES(5,4,5,'2020-08-20','2020-08-29')");
                    }
                }).allowMainThreadQueries()
                .build();
    }
}