package com.example.employeeroosterdemo.data;

import androidx.room.TypeConverter;

import java.sql.Date;
import java.util.Calendar;

public class Converters {

    @TypeConverter
    public static Date fromTimestamp(Long value) {
        return value == null ? null : new Date(value);
    }

    @TypeConverter
    public static Long dateToTimestamp(Date date) {
        return date == null ? null : date.getTime();
    }

}
