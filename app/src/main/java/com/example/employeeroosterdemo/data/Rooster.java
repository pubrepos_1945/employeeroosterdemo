package com.example.employeeroosterdemo.data;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.sql.Date;
import java.util.Objects;


@Entity(tableName = "Rooster")
public class Rooster {
    private static final int DEFAULT_WATERING_INTERVAL = 7;
    public Rooster(){}
    public void setId(int id) {
        Id = id;
    }

    public void setLocationId(int locationId) {
        LocationId = locationId;
    }

    public void setEmployeeId(int employeeId) {
        EmployeeId = employeeId;
    }

    public void setStartDate(@NonNull Date startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(@NonNull Date endDate) {
        this.endDate = endDate;
    }

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "id")
    private  int Id;

    @NonNull
    private  int LocationId;

    @NonNull
    private  int EmployeeId;

    @NonNull
    private  Date startDate;

    @NonNull
    private  Date endDate;

    @NonNull
    public int getId() {
        return Id;
    }

    @NonNull
    public int getLocationId() {
        return LocationId;
    }

    @NonNull
    public int getEmployeeId() {
        return EmployeeId;
    }

    @NonNull
    public Date getStartDate() {
        return startDate;
    }

    @NonNull
    public Date getEndDate() {
        return endDate;
    }

    public Rooster(@NonNull int id, @NonNull int locationId, @NonNull int employeeId, @NonNull Date startDate, @NonNull Date endDate) {
        Id = id;
        LocationId = locationId;
        EmployeeId = employeeId;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        return obj instanceof Locations
                && this.getId()== ((Locations) obj).getLocationsId();

    }

    @NonNull
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return new Rooster(getId(), getLocationId(),getEmployeeId(),getStartDate(),getEndDate());
    }


    @NonNull
    @Override
    public String toString() {
        return startDate.toString() + " - " +endDate.toString();
    }
}