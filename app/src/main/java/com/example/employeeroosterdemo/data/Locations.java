package com.example.employeeroosterdemo.data;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Objects;

@Entity(tableName = "Locations")
public class Locations {
    private static final int DEFAULT_WATERING_INTERVAL = 7;
    public Locations(){}
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "id")
    private  int LocationsId;

    @NonNull
    private  String LocationName;


    @NonNull
    public int getLocationsId() {
        return LocationsId;
    }

    @NonNull
    public String getLocationName() {
        return LocationName;
    }

    public void setLocationsId(int locationsId) {
        LocationsId = locationsId;
    }

    public void setLocationName(@NonNull String locationName) {
        LocationName = locationName;
    }

    public Locations(@NonNull int locationsId, @NonNull String locationsname) {
        LocationsId = locationsId;
        LocationName = locationsname;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getLocationsId());
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        return obj instanceof Locations
                && this.getLocationsId() == ((Locations) obj).getLocationsId();

    }

    @NonNull
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return new Locations(getLocationsId(), getLocationName());
    }

    @NonNull
    @Override
    public String toString() {
        return LocationName;
    }
}