package com.example.employeeroosterdemo.data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.sql.Date;
import java.util.List;

@Dao
public interface RoosterDAO {
    @Query("SELECT * FROM Rooster ORDER BY startDate")
    List<Rooster> getRoosters();

     @Query("SELECT * FROM Rooster WHERE startDate = :startDate and endDate = :endDate ORDER BY startDate")
    LiveData<List<Rooster>> getRoostersWithDates(Date startDate, Date endDate);

    @Query("SELECT * FROM Rooster WHERE :date between startDate and endDate  ORDER BY startDate")
    List<Rooster> getRoostersWithDate(Date date);

    @Query("SELECT * FROM Rooster WHERE id = :RoosterId")
    LiveData<Rooster> getRooster(String RoosterId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Rooster> Roosters);

}