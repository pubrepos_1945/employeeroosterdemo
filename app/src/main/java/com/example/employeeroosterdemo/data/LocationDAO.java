package com.example.employeeroosterdemo.data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface LocationDAO {
    @Query("SELECT * FROM Locations ORDER BY LocationName")
    LiveData<List<Locations>> getLocations();

    @Query("SELECT * FROM Locations ORDER BY LocationName")
    List<Locations> getLocations1();

    @Query("SELECT * FROM Locations WHERE id = :Id ORDER BY LocationName")
    LiveData<List<Locations>> getLocationsWithId(int Id);

    @Query("SELECT * FROM Locations WHERE id = :LocationsId")
    LiveData<Locations> getLocations(String LocationsId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Locations> Locations);

}