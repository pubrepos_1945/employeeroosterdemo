package com.example.employeeroosterdemo.data;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Objects;

@Entity(tableName = "Employee")
public class Employee {
    private static final int DEFAULT_WATERING_INTERVAL = 7;
    public Employee(){}
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "id")
    private  int EmployeeId;

    public void setEmployeeId(int employeeId) {
        EmployeeId = employeeId;
    }

    public void setEmployeeName(@NonNull String employeeName) {
        EmployeeName = employeeName;
    }

    @NonNull
    private  String EmployeeName;


    @NonNull
    public int getEmployeeId() {
        return EmployeeId;
    }

    @NonNull
    public String getEmployeeName() {
        return EmployeeName;
    }

    public Employee(@NonNull int employeeId, @NonNull String EmployeeName) {
        EmployeeId = employeeId;
        this.EmployeeName = EmployeeName;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getEmployeeId());
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        return obj instanceof Employee
                && this.getEmployeeId() == ((Employee) obj).getEmployeeId();

    }

    @NonNull
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return new Employee(getEmployeeId(), getEmployeeName());
    }

    @NonNull
    @Override
    public String toString() {
        return EmployeeName;
    }
}
