package com.example.employeeroosterdemo.data;

import android.os.Build;

import androidx.lifecycle.LiveData;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class RoosterRepository {
    private static RoosterRepository instance;
    private RoosterDAO RoosterDao;
    private EmployeeDAO EmployeeDao;
    private LocationDAO LocationsDao;
    private RoosterRepository(RoosterDAO roosterDAO,EmployeeDAO EmployeeDao, LocationDAO LocationsDao) {
        this.RoosterDao = roosterDAO;
        this.EmployeeDao = EmployeeDao;
        this.LocationsDao = LocationsDao;
    }

    public static RoosterRepository getInstance(RoosterDAO roosterDAO,EmployeeDAO EmployeeDao, LocationDAO LocationsDao) {
        if (instance == null) {
            synchronized (RoosterRepository.class) {
                if (instance == null) {
                    instance = new RoosterRepository(roosterDAO,EmployeeDao, LocationsDao);
                }
            }
        }
        return instance;
    }

    public List<Rooster> getRoosters() {
        return this.RoosterDao.getRoosters();
    }

    public LiveData<Rooster> getRooster(String RoosterId) {
        return this.RoosterDao.getRooster(RoosterId);
    }

    public List<Rooster> getRoostersWithDate(Date date) {
        return this.RoosterDao.getRoostersWithDate(date);
    }

    public List<String> getAllLocations(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return this.LocationsDao.getLocations1().stream().map(Locations::getLocationName).collect(Collectors.toList());
        }
        else{
            List<Locations> l = this.LocationsDao.getLocations1();
            List<String> ls = new ArrayList<>();
            for (Locations loc : l) {
                ls.add(loc.getLocationName());
            }
            return ls;
        }
    }

    public List<String> getEmployeesOnDate(Date date, int location) {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            List<Rooster> l = RoosterDao.getRoostersWithDate(date);
//            List<String> lids = l.stream().map(Rooster::getEmployeeId).map((a)->{return a.toString();}).collect(Collectors.toList());
//            return this.EmployeeDao.getEmployeesIn(String.join(",",lids)).stream().map(Employee::getEmployeeName).collect(Collectors.toList());
//        }
//        else{
            List<Rooster> l = new ArrayList<>();
            List<Rooster> l1 = RoosterDao.getRoosters();
            for (Rooster r : l1) {
                if(date.after(r.getStartDate()) && date.before(r.getEndDate())&& r.getLocationId() ==location){
                    l.add(r);
                }
            }
            List<String> lids = new ArrayList<>();
            for (Rooster r : l) {
                int i = r.getEmployeeId();
                lids.add(String.valueOf(i));
            }
            String ids= "";
            for (String s : lids) {
                    ids+= s+",";
            }
            if(ids.length()>0 && ids.contains(","))
            ids = ids.substring(0, ids.length()-1);
            List<Employee> le = this.EmployeeDao.getEmployeesIn(ids);
            List<String> res = new ArrayList<>();
            for (Employee e : le) {
                res.add(e.getEmployeeName());
            }

            return res;
//        }

    }
}
